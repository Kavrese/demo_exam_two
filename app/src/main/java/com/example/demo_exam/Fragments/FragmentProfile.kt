package com.example.demo_exam.Fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.demo_exam.AuthScreen.AuthActivity
import com.example.demo_exam.ChatListScreen.ChatListActivity
import com.example.demo_exam.R
import com.example.demo_exam.common.*
import kotlinx.android.synthetic.main.fragment_profile.*
import retrofit2.Call
import retrofit2.Response

class FragmentProfile: Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initRetrofit().create(Api::class.java).user()
            .enqueue(object : retrofit2.Callback<List<ModelUser>> {
                override fun onResponse(
                    call: Call<List<ModelUser>>,
                    response: Response<List<ModelUser>>
                ) {
                    name.text = "${response.body()!![0].firstname} ${response.body()!![0].lastname}"
                    email.text = "${response.body()!![0].email}"
                    if (response.body()!![0].avatar.isNotEmpty()) {
                        glideToImageView(requireContext(), response.body()!![0].avatar, ava)
                    }
                }

                override fun onFailure(call: Call<List<ModelUser>>, t: Throwable) {
                    showMessage(requireContext(), "Fail Get USER DATA")
                }

            })
        exit.setOnClickListener {
            startActivity(Intent(requireContext(), AuthActivity::class.java))
            requireActivity().finish()
        }

        chat_list.setOnClickListener {
//            Info.useUserChat = true
//            startActivity(Intent(requireContext(), ChatListActivity::class.java))
        }
    }
}