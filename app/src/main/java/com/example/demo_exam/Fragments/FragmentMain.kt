package com.example.demo_exam.Fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewpager2.widget.ViewPager2
import com.example.demo_exam.R
import com.example.demo_exam.common.*
import kotlinx.android.synthetic.main.fragment_main.*
import retrofit2.Call
import retrofit2.Response

class FragmentMain: Fragment() {
    lateinit var listFilms: List<ModelFilm>
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initRetrofit().create(Api::class.java).films().enqueue(object: retrofit2.Callback<List<ModelFilm>>{
            override fun onResponse(
                call: Call<List<ModelFilm>>,
                response: Response<List<ModelFilm>>
            ) {
                listFilms = response.body()!!
                pager.adapter = AdapterViewPager(listFilms)
                name_film.text = listFilms[0].name
            }

            override fun onFailure(call: Call<List<ModelFilm>>, t: Throwable) {
                showMessage(requireContext(), "Fail Get FILMS")
            }

        })

        pager.registerOnPageChangeCallback(object: ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                name_film.text = listFilms[position].name
            }
        })
    }
}