package com.example.demo_exam.ChatScreen

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.demo_exam.R
import com.example.demo_exam.common.*
import kotlinx.android.synthetic.main.activity_chat.*
import kotlinx.android.synthetic.main.activity_chat_list.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ChatActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)

        back_chat.setOnClickListener {
            finish()
        }
        name_film_chat.text = Info.modelChat.name

        getMessages()
    }

    private fun getMessages(){
        initRetrofit().create(Api::class.java).messages(id = Info.modelChat.chatId).enqueue(object:
            Callback<List<ModelMessage>>{
            override fun onResponse(
                call: Call<List<ModelMessage>>,
                response: Response<List<ModelMessage>>
            ) {
                rec_chat.apply {
                    layoutManager = LinearLayoutManager(this@ChatActivity, LinearLayoutManager.VERTICAL, true)
            //        adapter = AdapterChat()
                }
            }

            override fun onFailure(call: Call<List<ModelMessage>>, t: Throwable) {
                showMessage(this@ChatActivity, "Fail get Messages")
            }

        })
    }
}