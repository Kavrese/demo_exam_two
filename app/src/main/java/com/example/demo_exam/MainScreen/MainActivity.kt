package com.example.demo_exam.MainScreen

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.demo_exam.Fragments.FragmentColl
import com.example.demo_exam.Fragments.FragmentMain
import com.example.demo_exam.Fragments.FragmentProfile
import com.example.demo_exam.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        chooseFragment(FragmentMain())

        bottom.setOnNavigationItemSelectedListener {
            when(it.itemId){
                R.id.item1 -> chooseFragment(FragmentMain())
                //R.id.item2 -> chooseFragment(FragmentP())
                R.id.item3 -> chooseFragment(FragmentColl())
                R.id.item4 -> chooseFragment(FragmentProfile())
            }
            return@setOnNavigationItemSelectedListener true
        }
    }

    private fun chooseFragment (fragment: Fragment ) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.frame, fragment)
            .commit()
    }
}