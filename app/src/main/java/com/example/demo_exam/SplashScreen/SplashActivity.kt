package com.example.demo_exam.SplashScreen

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.example.demo_exam.AuthScreen.AuthActivity
import com.example.demo_exam.R
import com.example.demo_exam.RegScreen.RegActivity

class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Handler().postDelayed({
        if (getSharedPreferences("0", 0).getBoolean("isFirst", true)){
            startActivity(Intent(this, RegActivity::class.java))
            finish()
            getSharedPreferences("0", 0).edit()
                .putBoolean("isFirst", false)
                .apply()
        }else{
            startActivity(Intent(this, AuthActivity::class.java))
            finish()
        }
        }, 1500)
    }
}