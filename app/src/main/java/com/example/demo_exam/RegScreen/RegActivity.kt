package com.example.demo_exam.RegScreen

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.demo_exam.AuthScreen.AuthActivity
import com.example.demo_exam.MainScreen.MainActivity
import com.example.demo_exam.R
import com.example.demo_exam.common.*
import com.example.demo_exam.common.reg
import kotlinx.android.synthetic.main.activity_auth.*
import kotlinx.android.synthetic.main.activity_reg.*

class RegActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reg)

        reg.setOnClickListener {
            val textEmail = email_reg.text.toString()
            val textFirstname = firstname_reg.text.toString()
            val textLastname = lastname_reg.text.toString()
            val textPassword = password1_reg.text.toString()
            val textPassword2 = password2_reg.text.toString()
            if (textEmail.isNotEmpty() && textPassword.isNotEmpty() && textFirstname.isNotEmpty() && textLastname.isNotEmpty()){
                if (textPassword == textPassword2){
                    if (checkEmail(textEmail)) {
                        reg(
                            this,
                            textFirstname,
                            textLastname,
                            textEmail,
                            textPassword,
                            object : onLogin {
                                override fun onLogin(modelLogin: ModelLogin) {
                                    Info.token = modelLogin.token
                                    startActivity(
                                        Intent(
                                            this@RegActivity,
                                            MainActivity::class.java
                                        )
                                    )
                                    finish()
                                }
                            })
                    }else{
                        showMessage(this, "Bad Email")
                    }
                }else{
                    showMessage(this, "Passwords don't match")
                }
            }else{
                showMessage(this, "Null Text")
            }
        }

        to_auth.setOnClickListener {
            startActivity(Intent(this, AuthActivity::class.java))
            finish()
        }
    }
}