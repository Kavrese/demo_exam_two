package com.example.demo_exam.AuthScreen

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.demo_exam.MainScreen.MainActivity
import com.example.demo_exam.R
import com.example.demo_exam.RegScreen.RegActivity
import com.example.demo_exam.common.*
import kotlinx.android.synthetic.main.activity_auth.*

class AuthActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)

        enter.setOnClickListener {
            val textEmail = email_auth.text.toString()
            val textPassword = password_auth.text.toString()
            if (textEmail.isNotEmpty() && textPassword.isNotEmpty()){
                if (checkEmail(textEmail)) {
                    auth(this, textEmail, textPassword, object : onLogin {
                        override fun onLogin(modelLogin: ModelLogin) {
                            Info.token = modelLogin.token
                            startActivity(Intent(this@AuthActivity, MainActivity::class.java))
                            finish()
                        }
                    })
                }else{
                    showMessage(this, "Bad Email")
                }
            }else{
                showMessage(this, "Null Text")
            }
        }

        to_reg.setOnClickListener {
            startActivity(Intent(this@AuthActivity, RegActivity::class.java))
            finish()
        }
    }
}