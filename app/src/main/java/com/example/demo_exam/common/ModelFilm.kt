package com.example.demo_exam.common

data class ModelFilm(
    val movieId: String,
    val age: String,
    val name: String,
    val images: List<String>,
    val poster: String,
    val description: String,
)
