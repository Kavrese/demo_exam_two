package com.example.demo_exam.common

data class ModelChat (
    val name: String,
    val chatId: String
)
