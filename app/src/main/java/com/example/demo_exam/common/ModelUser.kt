package com.example.demo_exam.common

data class ModelUser(
    val userId: String,
    val firstname: String,
    val lastname: String,
    val email: String,
    val avatar: String
)
