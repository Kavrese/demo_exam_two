package com.example.demo_exam.common

data class ModelLogin(
    val token: String
)
