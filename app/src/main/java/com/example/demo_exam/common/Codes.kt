package com.example.demo_exam.common

import android.app.AlertDialog
import android.content.Context
import android.widget.ImageView
import com.bumptech.glide.Glide
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

fun showMessage(context: Context, title: String){
    AlertDialog.Builder(context)
        .setTitle(title)
        .setMessage("Try again")
        .setNegativeButton("OK"
        ) { dialog, which ->  }.show()
}

fun initRetrofit(): Retrofit{
    return Retrofit.Builder()
        .baseUrl("http://cinema.areas.su")
        .addConverterFactory(GsonConverterFactory.create())
        .build()
}

fun auth(context: Context, email: String, password: String, onLogin: onLogin){
    initRetrofit().create(Api::class.java).auth(email, password).enqueue(object: Callback<ModelLogin>{
        override fun onResponse(call: Call<ModelLogin>, response: Response<ModelLogin>) {
            if (response.body() != null){
                onLogin.onLogin(response.body()!!)
            }else{
                showMessage(context, "Fail auth NULL BODY ${response.message()}")
            }
        }

        override fun onFailure(call: Call<ModelLogin>, t: Throwable) {
            showMessage(context, "Fail auth ${t.message}")
        }

    })
}

fun reg(context: Context, firstname: String, lastname: String, email: String, password: String, onLogin: onLogin){
    initRetrofit().create(Api::class.java).reg(firstname, lastname, email, password).enqueue(object: Callback<ModelLogin>{
        override fun onResponse(call: Call<ModelLogin>, response: Response<ModelLogin>) {
            if (response.body() != null){
                auth(context, email, password, onLogin)
            }else{
                showMessage(context, "Fail reg NULL BODY")
            }
        }

        override fun onFailure(call: Call<ModelLogin>, t: Throwable) {
            auth(context, email, password, onLogin)
        }

    })
}

fun glideToImageView(context: Context, url: String, imageView: ImageView){
    Glide.with(context)
        .load("http://cinema.areas.su/up/images/$url")
        .into(imageView)
}

fun checkEmail(email: String): Boolean{
    val one = email.substringBefore("@")
    val two = email.substringAfter("@").substringBefore(".")
    val three = email.substringAfter(".")
    return "@" in email && "." in email && three.length <= 3 && one == one.toLowerCase() && two == two.toLowerCase() && three == three.toLowerCase()
}