package com.example.demo_exam.common

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.example.demo_exam.ChatListScreen.ChatListActivity
import com.example.demo_exam.ChatScreen.ChatActivity
import com.example.demo_exam.R

class AdapterViewPager(val listFilms: List<ModelFilm>):
    RecyclerView.Adapter<AdapterViewPager.ViewHolder>() {
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val img = itemView.findViewById<ImageView>(R.id.img_cover)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
       return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_view_pager, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        glideToImageView(holder.itemView.context, listFilms[position].poster, holder.img)
        holder.itemView.setOnClickListener{
            Info.model = listFilms[position]
            holder.itemView.context.startActivity(Intent(holder.itemView.context, ChatListActivity::class.java))
        }
    }

    override fun getItemCount(): Int = listFilms.size
}