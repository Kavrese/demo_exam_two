package com.example.demo_exam.common

data class ModelMessage (
val chatId: String,
val messageId: String,
val creationDateTime: String,
val firstName: String,
val lastName: String,
val avatar: String,
val text: String
)
