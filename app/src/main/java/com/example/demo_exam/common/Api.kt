package com.example.demo_exam.common

import retrofit2.Call
import retrofit2.http.*

interface Api {
    @POST("/auth/login")
    fun auth(@Query("email") email: String, @Query("password") password: String): Call<ModelLogin>

    @POST("/auth/register")
    fun reg(@Query("firstName") firstname: String, @Query("lastName") lastname: String,  @Query("email") email: String, @Query("password") password: String): Call<ModelLogin>

    @GET("/movies")
    fun films(@Query("filter") filter: String = "new"): Call<List<ModelFilm>>

    @GET("/user")
    fun user(@Header("Authorization") token: String = Info.token): Call<List<ModelUser>>

    @GET("/user/chats")
    fun chats(@Header("Authorization") token: String = Info.token): Call<List<ModelChat>>

    @GET("/chats/{id}/messages")
    fun messages(@Header("Authorization") token: String = Info.token, @Path("id") id: String): Call<List<ModelMessage>>

    @POST("/chats/{id}/messages")
    fun sendMessage(@Header("Authorization") token: String = Info.token, @Path("id") id: String, @Body body: ModelSendMessage): Call<ModelMessage>

    @GET("/chats/{id}")
    fun chatsFilm(@Path("id") id: String): Call<List<ModelChat>>

    @GET("/movies/{id}")
    fun film(@Path("id") id: String): Call<ModelFilm>
}