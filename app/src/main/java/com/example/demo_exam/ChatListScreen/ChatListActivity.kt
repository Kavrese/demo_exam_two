package com.example.demo_exam.ChatListScreen

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.demo_exam.R
import com.example.demo_exam.common.*
import kotlinx.android.synthetic.main.activity_chat_list.*
import retrofit2.Call
import retrofit2.Response

class ChatListActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat_list)

        back.setOnClickListener {
            finish()
        }

        initChats()
    }

    private fun initChats(){
        if (!Info.useUserChat) {
            initRetrofit().create(Api::class.java).chatsFilm(Info.model.movieId)
                .enqueue(object : retrofit2.Callback<List<ModelChat>> {
                    override fun onResponse(
                        call: Call<List<ModelChat>>,
                        response: Response<List<ModelChat>>
                    ) {
                        rec_chats.apply {
                            adapter = ChatListAdapter(response.body()!!)
                            layoutManager = LinearLayoutManager(this@ChatListActivity)
                        }
                    }

                    override fun onFailure(call: Call<List<ModelChat>>, t: Throwable) {
                        showMessage(this@ChatListActivity, "Fail get list chats")
                    }

                })
        }else{
            Info.useUserChat = false
            initRetrofit().create(Api::class.java).chats()
                .enqueue(object : retrofit2.Callback<List<ModelChat>> {
                    override fun onResponse(
                        call: Call<List<ModelChat>>,
                        response: Response<List<ModelChat>>
                    ) {
                        rec_chats.apply {
                            adapter = ChatListAdapter(response.body()!!)
                            layoutManager = LinearLayoutManager(this@ChatListActivity)
                        }
                    }

                    override fun onFailure(call: Call<List<ModelChat>>, t: Throwable) {
                        showMessage(this@ChatListActivity, "Fail get list chats")
                    }

                })
        }
    }
}