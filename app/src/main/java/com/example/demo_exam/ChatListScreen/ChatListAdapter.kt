package com.example.demo_exam.ChatListScreen

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.demo_exam.ChatScreen.ChatActivity
import com.example.demo_exam.R
import com.example.demo_exam.common.*
import retrofit2.Call
import retrofit2.Response

class ChatListAdapter(val list: List<ModelChat>) : RecyclerView.Adapter<ChatListAdapter.ViewHolder>() {
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val ava_chat = itemView.findViewById<ImageView>(R.id.ava_chat)
        val text_ava_chat = itemView.findViewById<TextView>(R.id.text_ava)
        val name_chat = itemView.findViewById<TextView>(R.id.chat_name)
        val last_name = itemView.findViewById<TextView>(R.id.name_last_message)
        val last_message = itemView.findViewById<TextView>(R.id.last_message)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_chat, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.name_chat.text = list[position].name
        if (Info.model.poster.isNotEmpty()){
            glideToImageView(holder.itemView.context, Info.model.poster, holder.ava_chat)
        }else{
            val name_film = Info.model.name
            holder.text_ava_chat.visibility = View.VISIBLE
            holder.text_ava_chat.text =
                when(name_film.split(" ").size){
                    1 -> name_film.substring(0, 2).toUpperCase()
                    else -> "${name_film.split(" ")[0][0]}  ${name_film.split(" ")[1][0]}".toUpperCase()
            }
        }

        initRetrofit().create(Api::class.java).messages(id = list[position].chatId).enqueue(object: retrofit2.Callback<List<ModelMessage>>{
            override fun onResponse(
                call: Call<List<ModelMessage>>,
                response: Response<List<ModelMessage>>
            ) {
                val last = response.body()!![response.body()!!.lastIndex]
                holder.last_name.text = "${last.firstName} ${last.lastName}: "
                holder.last_message.text = last.text
            }

            override fun onFailure(call: Call<List<ModelMessage>>, t: Throwable) {
                showMessage(holder.itemView.context, "Fail Get Last Message")
            }

        })

        holder.itemView.setOnClickListener {
            Info.modelChat = list[position]
            holder.itemView.context.startActivity(Intent(holder.itemView.context, ChatActivity::class.java))
        }
    }

    override fun getItemCount(): Int = list.size

}
